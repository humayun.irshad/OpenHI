import pandas as pd
import mysql.connector

# Read manifest
def read_file(fpath):
    with open(fpath) as f:
        header = f.readline()[:-1].split('\t')
        print(header)
        contents = [line[:-1].split('\t')[1].split('.')[0].split('_') for line in f.readlines()]
        # contents = [line[:-1].split('_')[0].split('\t') for line in f.readlines()]
        print(type(contents[0]))

    return header, contents


# build connector, s = 'local' or 'server'
def create_connector(s):
    """Connect to MySQL server and database based on preset values. Return database object."""
    if s == 'local':
        mydatabase = mysql.connector.connect(
            unix_socket = '/Applications/MAMP/tmp/mysql/mysql.sock',
            user='root',
            passwd='root',
            database='db_wsi1_dev'
        )
    elif s == 'server':
        mydatabase = mysql.connector.connect(
            host='192.168.30.160',
            user='march-mac',
            passwd='123456',
            database='db_wsi1_dev'
        )

    print(mydatabase)

    return mydatabase


mani_file = '../framework_src/manifest2.txt'
header, contents = read_file(mani_file)
# db = create_connector('local')
#
# sql = 'insert into wsi (tcga_case_id, tcga_wsi_id, tcga_uuid) ' \
#           'VALUES (%s, %s, %s)'
# mycursor = db.cursor()
# mycursor.executemany(sql, contents)

# have already insert!!!
# db.commit()
# mycursor.close()

