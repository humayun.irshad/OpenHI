-- Create the schema
CREATE DATABASE `db_wsi1_dev` ; -- OR `db_wsi1`

USE `db_wsi1_dev`

CREATE TABLE `annotator` (
  `annotator_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `codename` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NA',
  PRIMARY KEY (`annotator_id`)
) ENGINE=InnoDB ;

CREATE TABLE `wsi` (
  `slide_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `tcga_wsi_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `tcga_case_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  PRIMARY KEY (`slide_id`)
) ENGINE=InnoDB ;

CREATE TABLE `grading` (
  `grading_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `grading_std_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  PRIMARY KEY (`grading_id`)
) ENGINE=InnoDB ;

CREATE TABLE `pslv` (
  `pslv_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `subregion_density` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pslv_id`)
) ENGINE=InnoDB ;

CREATE TABLE `point` (
  `pt_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned DEFAULT NULL,
  `annotation_ts` datetime NOT NULL,
  `grading_id` tinyint(3) unsigned NOT NULL,
  `slide_id` smallint(5) unsigned NOT NULL,
  `annotator_id` smallint(5) unsigned NOT NULL,
  `pslv_id` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`pt_id`),
  KEY `INDEX` (`x`,`y`) USING BTREE,
  KEY `annotator_id_idx` (`annotator_id`),
  KEY `slide_id_idx` (`slide_id`),
  KEY `grading_id_idx` (`grading_id`),
  KEY `pslv_id_idx` (`pslv_id`),
  CONSTRAINT `annotator_id` FOREIGN KEY (`annotator_id`) REFERENCES `annotator` (`annotator_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `grading_id` FOREIGN KEY (`grading_id`) REFERENCES `grading` (`grading_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pslv_id` FOREIGN KEY (`pslv_id`) REFERENCES `pslv` (`pslv_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `slide_id` FOREIGN KEY (`slide_id`) REFERENCES `wsi` (`slide_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;
