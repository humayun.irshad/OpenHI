-- This is a utility script to empty the point table in the WSI-Annotator data model.
--    Created on 20180705
--    Developed by Pargorn Puttapirat - March (pargorn@stu.xjtu.edu.cn) at XJTU

use `db_wsi4_dev`;
delete from patient;

USE `db_wsi4_dev`;
CREATE TABLE `wsi` (
  `slide_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `tcga_wsi_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `tcga_case_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `tcga_wsi_slide_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  PRIMARY KEY (`slide_id`),
  CONSTRAINT `tcga_case_id_in_wsi` FOREIGN KEY (`tcga_case_id`) REFERENCES `patient` (`tcga_case_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


-- INSERT INTO `db_wsi1_dev`.`grading` (`grading_std_name`) VALUES ('five');
