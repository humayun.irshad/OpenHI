# This is development function to call WSI using openslide and view it in OpenCV window.
import anno_img as img
from anno_img import LoaderConf
import cv2 as cv
import openslide
import numpy as np
import os
import anno_web as beproc
# from matplotlib import pyplot as plt


def imshow(input_cv_img):
    # imshow area
    cv.imshow('Figure', input_cv_img)
    cv.waitKey(0)
    cv.destroyAllWindows()


# setup some static values
# top_left_coor = (200, 200)
# viewing_size = (1000, 1000)
# showing_size = (500, 500)

# Initialize the loader
# Program static settings
# pathToDatabase = '/home1/pp/docs/gdc-downloads/TCGA-KIRC-WSI/'
pathToDatabase = os.getcwd() + '/framework_src/'

# Initialization of the framework
loader = LoaderConf(pathToDatabase)

#   Create labelling matrix full path
superpixelLevel = loader.superpixelLv

# Input from ST#3: (OR initial values?)
fileNumber = 96
# Specify the viewer coordinates
tl_coor = (15770, 16141)
tl_coor = np.add(tl_coor, 500)
viewing_coor = (17436-tl_coor[0], 17807-tl_coor[1])
viewer_size = (1000, 1000)
# ----- [End] Input from ST#3 ----- #

# Reconfigure the loader
loader.gen_filename(fileNumber)
print('Loader bwboun fullpath: ')
print(loader.fullpathBWBoun)
loader.bwbown_add(img.load_bwboundary(loader.fullpathBWBoun, 0))  # Get bw boundary
loader.wsi_update_ptr_with_filename()

i_wsi = img.call_wsi(loader, tl_coor, viewing_coor, viewer_size)

bw_crop = img.img_crop(loader.imgBWBoun[loader.current_pslv], tl_coor, viewing_coor, 0)
bw_boun = bw_crop.copy()

# given coordinate
loop_coor = (16769, 17116)
loop_coor_r = np.subtract(loop_coor, tl_coor)

h, w = bw_crop.shape     # 1 layer image

mask = np.zeros((h + 2, w + 2), np.uint8)

mask[:] = 0
print('Image size: ' + str(bw_crop.shape))

# Add seedpoint as a marker to the image
cv.floodFill(bw_crop, mask, tuple(loop_coor_r.astype(int)), 128)
# cv.circle(bw_crop, tuple(loop_coor_r.astype(int)), 25, 255, -1)
# imshow(bw_crop)

bw_crop_size = cv.resize(bw_crop, viewer_size, interpolation=cv.INTER_CUBIC)
# imshow(bw_crop_size)

# -- Create the ff_only image
# bw_boun2gray = cv.cvtColor(bw_boun, cv.COLOR_BGR2GRAY)
img_inv_boundary = cv.bitwise_not(bw_boun)
# imshow(img_inv_boundary)

ret, bw_ff = cv.threshold(bw_crop, 10, 255, cv.THRESH_BINARY)
img_ff_only = cv.bitwise_and(img_inv_boundary, bw_ff)

# Make RGB of the mask
mask_ff_only_rgb = cv.cvtColor(img_ff_only, cv.COLOR_GRAY2RGB)

# -- Blend with color and alpha
# Make mask as color
ff_only_mask = np.zeros_like(mask_ff_only_rgb)
ff_only_mask[:, :, 0] = img_ff_only*1
ff_only_mask[:, :, 1] = img_ff_only*0
ff_only_mask[:, :, 2] = img_ff_only*1

wsi_shape = i_wsi.shape
ff_only_mask_size = cv.resize(ff_only_mask, wsi_shape[0:2])

alpha = 0.3
beta = 1 - alpha

i_blend = cv.addWeighted(ff_only_mask_size, alpha, i_wsi, beta, 0)
imshow(i_blend)

# The 'i_blend' should go with the 'img_ff_only' mask

# -- Crate blend_bwboun color (already have mask)

