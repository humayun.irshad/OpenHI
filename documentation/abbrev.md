# Abbreviations in the project's code

| Abbrev | Definition |
| ------ | ---------- |
| coor | coordinate | 
| wsi | whole-slide image |
| db | database |
| anno | annotation *OR* annotator |
| dev | development | 
| ff | flood-fill (image processing) |
| assoc | associate(d) | 
| loader | WSI loader |
| rand | random |
| pt | point |
| pslv | pre-segmentation level |
| s-obj | static object (refer to attribute of annotating point(s) | 
| d-obj | dynamic object (refer to annotating point(s) |
| tl | top-left |
| br | bottom-right |
| dop | developer option |
| uuid | unique identification |
| fn | filename |
| BW | black and white | 
| Boun/boun | boundary/boundaries |
| tog | toggle |
| ptr | pointer (usually WSI's pointer) |
| mani | file manifest |
| mod | modify *OR* modification | 
| inv | invert *OR* inverting |
| loc | local |
| img or i| image |
| inter | interface |
| vp | view position |

## General abbreviation

| Abbrev | Definition |
| ------ | ---------- |
| cv | OpenCV Library | 
| np | Numpy |
| sk | scikit (usually scikit-image) |

