# Documentation for front-end to back-end interface

# Documentation for the front-end JS
The front-end involves 1 template HTML file which is located at `/templates/mytemplate.html`. Currently, other files in the `templates` directory is for exploratory or development purposes. 

## Prerequisite
* Programming language: Javascript
* jQuery
* OpenSeadragon Library (OSD)


### Updating the viewing image with new (or old) viewing coordinates
*Request to back-end side*<br />

URL: `/_update_image` <br />
*Passing to back-end* <br />

| Variable Name | Type | Description |
| --- | --- | --- |
| var1 | int | x of top-left coordinate | 
| var2 | int | y of top-left coordinate |
| var3 | int | x of bottom-right coordinate |
| var4 | int | y of bottom-right coordinate |
| var5 | int | width of viewer size (output image size) |
| var6 | int | height of viewer size (output image size) |
| var7 | int | level of pre-segmentation |
| var8 | string | (tuple of tuple(s)) of latest invalid points | 

*Example of var8:* ((x_1, y_1), (x_2, y_2), ... (x_n, y_n))

Triggering this function could be done be two ways. 
1. Pan and zoom the image in the main viewer. 
2. Use the debugging option to specify exact x and y coordinate with image width. 
Noted that the image height will be automatically calculated based on the specified
width (in the input box) and specified viewer size. 

*Returning to front-end* <br />
(All of them are strings, JSON format) <br />

| Variable Name | Type | Description |
| --- | --- | --- |
| slide_url | string | URL of the new slide | 
| img_size_width | int | Full image width |
| img_size_height | int | Full image height |
| top_left_x | int | x of top-left coordinate [Repeat the input] |
| top_left_y | int | y of top-left coordinate [Repeat the input] |
| viewing_size_x | int | x of bottom-right coordinate |
| viewing_size_y | int | y of bottom-right coordinate |
| request_status | string | 0 = The new image is updated successfully <br /> 1 = The viewing size is less than 1 <br /> 2 = Did not successfully update URL, update image, or remove old image.    


### Let front-end get the info of the current WSI
**URL:** `/_get_info`<br />
*Returning*<br />

| Variable Name | Description |
| --- | --- |
| img_width | Total WSI width | 
| img_height | Total WsI height | 
| ps_lv | maximum pre-segmentation level <br /> (max = 9, min = 1) | 

### Recording the annotating points
**URL:** `/_record` <br />
methods=['POST'] <br />
*Input*

```
{"id":{
    "x":<int>
    "y":<int>
    "grading":<int[1,4]>
    "ps_lv":<int>
    }
}
```

**Meaning of the values**

| Variable | Value | Meaning | 
| --- | --- | --- | 
| x | *int* | x coordinate |
| y | *int* | y coordinate |
| grading | *int*: 0 | healthy tissue |
| grading | *int*: 1-4 | diseased tissue |
| ps_lv | *int*: (1-n) | pre-segmentation level |

*Returning* <br />
status = {'record successful'} > Return value if the request JSON is read successfully.

### Request to undo annotation
**URL:** `/_undo` <br />
*Input*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| cmd | int | The number of times the user wants to undo the annotation 

*Output*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| status | string | successful



### Request to update annotator id
**URL:** `/_annotator_id` <br />
*Input*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| id | int | id of the annotator |

*Output*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| status | string | successful | 
| result | string | current slide id |

### Request to update slide id
**URL:** `/_change_slide_id` <br />
*Input*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| id | int | id of the new slide | 

*Output*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| status | string | successful |
| result | string | current slide id |

### Toggle switch for showing sub-region boundary
**URL:** `/_tog_boun` <br />
*Input*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| cmd | int | command to the framework, value always = 1 | 

*Output*

| Variable Name | Type | Description | 
| --- | --- | --- | 
| status | string | successful |
| result | string | current the switch |