"""This is TCGA WSI connector (fullpath caller). This script is not meant to be run as a main function."""
import json
import os

# Load configuration JSON file
with open('/home1/pp/docs/py-proj/flask-anno2/img_preproc_opensource_opensource/tcga_connector_conf.json') as json_data_file:
    conf = json.load(json_data_file)


# Function to provide fullpath to the WSI.
def get_fullpath(slide_number):
    """
    This function will return fullpath of the WSI according to the given slide number.
    :param slide_number: the requesting slide number
    :return: a string of full path to the requested WSI file
    """
    # Load manifest file
    # (note) !! Manifest file is always named 'manifest2.txt'
    fn_mani = conf['fullpath-manifest']

    # Load manifest
    file_stream = open(fn_mani, "r")

    num_of_file_counter = 0
    # Get and display header of the file (not used)
    mani_header = file_stream.readline()

    # Prepare the parameters
    uuid = []
    fn_wsi = []
    md5 = []
    size = []
    state = []

    for line in file_stream:
        loop_line = line
        # print(len(loop_line))
        loop_a = loop_line.split("\t")
        # print(loop_line.split("\t"))

        uuid.append(loop_a[0])
        fn_wsi.append(loop_a[1])
        md5.append(loop_a[2])
        size.append(loop_a[3])
        state.append(loop_a[4])

        num_of_file_counter += 1

    # result: uuid, fn_wsi, size

    print('Slide number = ' + str(slide_number) + ' , generating fullpath')

    # ----- File name organizing area ----- #
    filesep = os.sep

    # Create a path to WSI
    fullpath_wsi = conf['fullpath-parent'] + filesep + uuid[slide_number] + filesep + fn_wsi[slide_number]

    return fullpath_wsi


if __name__ == '__main__':
    print('This script is not meant to be run as a main function. Please refer to the documentation')


