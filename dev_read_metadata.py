# This development script is made as an example on how to read WSI's metadata.
#   @march: Created on 20180823

# It consists of
#   * Normal initizliation of the framework
#   * OpenSlide properties reading (For the support list, please refer to:
#       https://openslide.org/api/python/#standard-properties)

# ----- [ Framework initialization ] ----- #
import numpy as np
import os
import anno_img as img
import anno_web as web
import anno_sql as sql
from anno_web import Clr

pathToDatabase = os.getcwd() + '/framework_src/'

# Initialization of the framework
loader = img.LoaderConf(pathToDatabase)
rand_url = web.WebInter()
db = sql.create_connector()
s_obj = sql.CurrentStaticObject()
pt = sql.PointDynamicObject()

# Temp static value
superpixelLevel = loader.superpixelLv

pt.update_pslv(loader.current_pslv)
print('pt.pslv = %s'%pt.pslv)

# Input from ST#3: (OR initial values?)
fileNumber = 96
s_obj.slide_id = fileNumber + 1
# Specify the viewing properties
tl_coor = (10000, 3000)
viewing_coor = (1000, 1000)
viewer_size = (1000, 1000)
# ----- [End] Input from ST#3 ----- #

# Reconfigure the loader
loader.gen_filename(fileNumber)
print('setting r_id...')
loader.get_maxRegionID(fileNumber, db)
print('setting anno_batch...')

print('-----------call laoder.get_maxAnno in main(app.py)')
loader.get_maxAnnobatch(db, pt.pslv, s_obj)

for i in range(loader.superpixelLv):
    loader.bwbown_add(img.load_bwboundary(loader.fullpathBWBoun[i], 0), i)  # Get bw boundary

loader.wsi_update_ptr_with_filename()

if loader.dopVerbose:
    wd = os.getcwd()  # Get a working directory
    print('Working dir: ' + wd)

# Load the first WSIs (no overlaying layer)
img.call_wsi(loader, tl_coor, viewing_coor, viewer_size)
img.wsi_get_thumbnail(loader, rand_url.current, viewer_size)
print('first image is saved as: ' + rand_url.current)

# ----- [ Getting slide properties ] ----- #

# Print all available properties
list_of_properties = list(loader.ptr.properties)    # get the list
print('\n This is the start of the list: ')
for line in list_of_properties:
    print(line)

# Example on how to get the value of the property
#   Property from the vendor (aperio.*)
print('The magnification of the WSI is: ')
print(loader.ptr.properties['aperio.AppMag'])

#   Standard property from OpenSlide (openslide.*)
# (full reference: https://openslide.org/api/python/#standard-properties)
print('Comment that OpenSlide can read from: ')
print(loader.ptr.properties['openslide.comment'])

print('Original WSI height (level 0): ')
print(loader.ptr.properties['openslide.level[0].height'])


